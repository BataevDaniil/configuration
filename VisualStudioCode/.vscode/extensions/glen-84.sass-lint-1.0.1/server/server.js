"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protocol_configuration_proposed_1 = require("vscode-languageserver-protocol/lib/protocol.configuration.proposed");
const vscode_languageserver_1 = require("vscode-languageserver");
const fs = require("fs");
const globule = require("globule");
const path = require("path");
const vscode_uri_1 = require("vscode-uri");
// Create a connection for the server. The connection uses Node's IPC as a transport.
const connection = vscode_languageserver_1.createConnection(vscode_languageserver_1.ProposedFeatures.all);
// Create a simple text document manager. The text document manager supports full document sync only.
const documents = new vscode_languageserver_1.TextDocuments();
// Make the text document manager listen on the connection for open, change, and close text document events.
documents.listen(connection);
class SettingsCache {
    constructor() {
        this.uri = undefined;
        this.promise = undefined;
    }
    get(uri) {
        return __awaiter(this, void 0, void 0, function* () {
            if (uri === this.uri) {
                trace(`SettingsCache: cache hit for: ${this.uri}`);
                // tslint:disable-next-line:no-non-null-assertion
                return this.promise;
            }
            if (hasConfigurationCapability) {
                this.uri = uri;
                return this.promise = new Promise((resolve, _reject) => __awaiter(this, void 0, void 0, function* () {
                    trace(`SettingsCache: cache updating for: ${this.uri}`);
                    const configRequestParam = { items: [{ scopeUri: uri, section: "sasslint" }] };
                    const settings = yield connection.sendRequest(protocol_configuration_proposed_1.ConfigurationRequest.type, configRequestParam);
                    resolve(settings[0]);
                }));
            }
            this.promise = Promise.resolve(globalSettings);
            return this.promise;
        });
    }
    flush() {
        this.uri = undefined;
        this.promise = undefined;
    }
}
let workspaceFolders;
let hasConfigurationCapability = false;
let hasWorkspaceFolderCapability = false;
let globalSettings;
const settingsCache = new SettingsCache();
// Map stores undefined values to represent failed resolutions.
const globalPackageManagerPath = new Map();
const path2Library = new Map();
const document2Library = new Map();
let configPathCache = {};
const CONFIG_FILE_NAME = ".sass-lint.yml";
// tslint:disable-next-line:no-namespace
var NoSassLintLibraryRequest;
(function (NoSassLintLibraryRequest) {
    NoSassLintLibraryRequest.type = new vscode_languageserver_1.RequestType("sass-lint/noLibrary");
})(NoSassLintLibraryRequest || (NoSassLintLibraryRequest = {}));
function trace(message, verbose) {
    connection.tracer.log(message, verbose);
}
connection.onInitialize((params) => {
    trace("onInitialize");
    if (params.workspaceFolders) {
        workspaceFolders = params.workspaceFolders;
        // Sort folders.
        sortWorkspaceFolders();
    }
    const capabilities = params.capabilities;
    hasWorkspaceFolderCapability =
        capabilities.workspace &&
            !!capabilities.workspace.workspaceFolders;
    hasConfigurationCapability =
        capabilities.workspace &&
            !!capabilities.workspace.configuration;
    return {
        capabilities: {
            textDocumentSync: documents.syncKind
        }
    };
});
connection.onInitialized(() => {
    if (hasWorkspaceFolderCapability) {
        connection.workspace.onDidChangeWorkspaceFolders((event) => {
            trace("onDidChangeWorkspaceFolders");
            // Removed folders.
            for (const workspaceFolder of event.removed) {
                const index = workspaceFolders.findIndex((folder) => folder.uri === workspaceFolder.uri);
                if (index !== -1) {
                    workspaceFolders.splice(index, 1);
                }
            }
            // Added folders.
            for (const workspaceFolder of event.added) {
                workspaceFolders.push(workspaceFolder);
            }
            // Sort folders.
            sortWorkspaceFolders();
        });
    }
});
function sortWorkspaceFolders() {
    workspaceFolders.sort((folder1, folder2) => {
        let uri1 = folder1.uri.toString();
        let uri2 = folder2.uri.toString();
        if (!uri1.endsWith("/")) {
            uri1 += path.sep;
        }
        if (uri2.endsWith("/")) {
            uri2 += path.sep;
        }
        return (uri1.length - uri2.length);
    });
}
documents.onDidOpen((event) => __awaiter(this, void 0, void 0, function* () {
    trace(`onDidOpen: ${event.document.uri}`);
    validateTextDocument(event.document);
}));
// The content of a text document has changed.
// This event is emitted when the text document is first opened or when its content has changed.
documents.onDidChangeContent((event) => __awaiter(this, void 0, void 0, function* () {
    trace(`onDidChangeContent: ${event.document.uri}`);
    const settings = yield settingsCache.get(event.document.uri);
    if (settings && settings.run === "onType") {
        validateTextDocument(event.document);
    }
    else if (settings && settings.run === "onSave") {
        // Clear the diagnostics when validating on save and when the document is modified.
        connection.sendDiagnostics({ uri: event.document.uri, diagnostics: [] });
    }
}));
documents.onDidSave((event) => __awaiter(this, void 0, void 0, function* () {
    trace(`onDidSave: ${event.document.uri}`);
    const settings = yield settingsCache.get(event.document.uri);
    if (settings && settings.run === "onSave") {
        validateTextDocument(event.document);
    }
}));
// A text document was closed.
documents.onDidClose((event) => {
    trace(`onDidClose: ${event.document.uri}`);
    connection.sendDiagnostics({ uri: event.document.uri, diagnostics: [] });
    document2Library.delete(event.document.uri);
    delete configPathCache[vscode_uri_1.default.parse(event.document.uri).fsPath];
});
function loadLibrary(docUri) {
    return __awaiter(this, void 0, void 0, function* () {
        trace(`loadLibrary for: ${docUri}`);
        const uri = vscode_uri_1.default.parse(docUri);
        let promise;
        const settings = yield settingsCache.get(docUri);
        const getGlobalPath = () => getGlobalPackageManagerPath(settings.packageManager);
        if (uri.scheme === "file") {
            const file = uri.fsPath;
            const directory = path.dirname(file);
            if (settings && settings.nodePath) {
                promise = vscode_languageserver_1.Files.resolve("sass-lint", settings.nodePath, settings.nodePath, trace).then(undefined, () => vscode_languageserver_1.Files.resolve("sass-lint", getGlobalPath(), directory, trace));
            }
            else {
                promise = vscode_languageserver_1.Files.resolve("sass-lint", getGlobalPath(), directory, trace);
            }
        }
        else {
            // tslint:disable-next-line:no-non-null-assertion -- "cwd" argument can be undefined.
            promise = vscode_languageserver_1.Files.resolve("sass-lint", getGlobalPath(), undefined, trace);
        }
        document2Library.set(docUri, promise.then((path) => {
            let library;
            if (!path2Library.has(path)) {
                library = require(path);
                trace(`sass-lint library loaded from: ${path}`);
                path2Library.set(path, library);
            }
            return path2Library.get(path);
        }, () => {
            connection.sendRequest(NoSassLintLibraryRequest.type, { source: { uri: docUri } });
            return undefined;
        }));
    });
}
function validateTextDocument(document) {
    return __awaiter(this, void 0, void 0, function* () {
        const docUri = document.uri;
        trace(`validateTextDocument: ${docUri}`);
        // Sass Lint can only lint files on disk.
        if (vscode_uri_1.default.parse(docUri).scheme !== "file") {
            return;
        }
        const settings = yield settingsCache.get(docUri);
        if (settings && !settings.enable) {
            return;
        }
        if (!document2Library.has(document.uri)) {
            yield loadLibrary(document.uri);
        }
        if (!document2Library.has(document.uri)) {
            return;
        }
        const library = yield document2Library.get(document.uri);
        if (library) {
            try {
                const diagnostics = yield doValidate(library, document);
                connection.sendDiagnostics({ uri: docUri, diagnostics });
            }
            catch (err) {
                connection.window.showErrorMessage(getErrorMessage(err, document));
            }
        }
    });
}
function validateAllTextDocuments(textDocuments) {
    const tracker = new vscode_languageserver_1.ErrorMessageTracker();
    for (const document of textDocuments) {
        try {
            validateTextDocument(document);
        }
        catch (err) {
            tracker.add(getErrorMessage(err, document));
        }
    }
    tracker.sendErrors(connection);
}
function doValidate(library, document) {
    return __awaiter(this, void 0, void 0, function* () {
        trace(`doValidate: ${document.uri}`);
        const diagnostics = [];
        const docUri = document.uri;
        const uri = vscode_uri_1.default.parse(docUri);
        if (vscode_uri_1.default.parse(docUri).scheme !== "file") {
            // Sass Lint can only lint files on disk.
            trace("No linting: file is not saved on disk");
            return diagnostics;
        }
        const settings = yield settingsCache.get(docUri);
        if (!settings) {
            trace("No linting: settings could not be loaded");
            return diagnostics;
        }
        const configFile = yield getConfigFile(docUri);
        trace(`Config file: ${configFile}`);
        const compiledConfig = library.getConfig({}, configFile);
        const filePath = uri.fsPath;
        let relativePath;
        if (configFile && settings.resolvePathsRelativeToConfig) {
            relativePath = path.relative(path.dirname(configFile), filePath);
        }
        else {
            relativePath = getWorkspaceRelativePath(filePath);
        }
        trace(`Absolute path: ${filePath}`);
        trace(`Relative path: ${relativePath}`);
        if (globule.isMatch(compiledConfig.files.include, relativePath) &&
            !globule.isMatch(compiledConfig.files.ignore, relativePath)) {
            const result = library.lintText({
                text: document.getText(),
                format: path.extname(filePath).slice(1),
                filename: filePath
            }, {}, configFile);
            for (const msg of result.messages) {
                diagnostics.push(makeDiagnostic(msg));
            }
        }
        else {
            trace(`No linting: file "${relativePath}" is excluded`);
        }
        return diagnostics;
    });
}
function getConfigFile(docUri) {
    return __awaiter(this, void 0, void 0, function* () {
        const filePath = vscode_uri_1.default.parse(docUri).fsPath;
        let configFile = configPathCache[filePath];
        if (configFile) {
            trace(`Config path cache hit for: ${filePath}`);
            return configFile;
        }
        else {
            trace(`Config path cache miss for: ${filePath}`);
            const dirName = path.dirname(filePath);
            configFile = locateFile(dirName, CONFIG_FILE_NAME);
            if (configFile) {
                // Cache.
                configPathCache[filePath] = configFile;
                return configFile;
            }
        }
        const settings = yield settingsCache.get(docUri);
        if (settings && settings.configFile) {
            // Cache.
            configPathCache[filePath] = settings.configFile;
            return settings.configFile;
        }
        return null;
    });
}
function locateFile(directory, fileName) {
    let parent = directory;
    do {
        directory = parent;
        const location = path.join(directory, fileName);
        try {
            fs.accessSync(location, fs.constants.R_OK);
            return location;
        }
        catch (e) {
            // Do nothing.
        }
        parent = path.dirname(directory);
    } while (parent !== directory);
    return null;
}
;
function getWorkspaceRelativePath(filePath) {
    if (workspaceFolders) {
        for (const workspaceFolder of workspaceFolders) {
            let folderPath = vscode_uri_1.default.parse(workspaceFolder.uri).fsPath;
            if (!folderPath.endsWith("/")) {
                folderPath += path.sep;
            }
            if (folderPath && filePath.startsWith(folderPath)) {
                return path.relative(folderPath, filePath);
            }
        }
    }
    return filePath;
}
function makeDiagnostic(msg) {
    let severity;
    switch (msg.severity) {
        case 1:
            severity = vscode_languageserver_1.DiagnosticSeverity.Warning;
            break;
        case 2:
            severity = vscode_languageserver_1.DiagnosticSeverity.Error;
            break;
        default:
            severity = vscode_languageserver_1.DiagnosticSeverity.Information;
            break;
    }
    let line;
    if (msg.line) {
        line = msg.line - 1;
    }
    else {
        line = 0;
    }
    let column;
    if (msg.column) {
        column = msg.column - 1;
    }
    else {
        column = 0;
    }
    let message;
    if (msg.message) {
        message = `${msg.message} (${msg.ruleId})`;
    }
    else {
        message = "Unknown error.";
    }
    return {
        severity,
        range: {
            start: { line, character: column },
            end: { line, character: column + 1 }
        },
        message,
        source: "sass-lint"
    };
}
function getErrorMessage(err, document) {
    let errorMessage = "unknown error";
    if (typeof err.message === "string" || err.message instanceof String) {
        errorMessage = err.message;
    }
    const fsPath = vscode_languageserver_1.Files.uriToFilePath(document.uri);
    const message = `vscode-sass-lint: '${errorMessage}' while validating: ${fsPath} stacktrace: ${err.stack}`;
    return message;
}
function getGlobalPackageManagerPath(packageManager) {
    trace(`Begin - resolve global package manager path for: ${packageManager}`);
    if (!globalPackageManagerPath.has(packageManager)) {
        let path;
        if (packageManager === "npm") {
            path = vscode_languageserver_1.Files.resolveGlobalNodePath(trace);
        }
        else if (packageManager === "yarn") {
            path = vscode_languageserver_1.Files.resolveGlobalYarnPath(trace);
        }
        // tslint:disable-next-line:no-non-null-assertion
        globalPackageManagerPath.set(packageManager, path);
    }
    trace(`Done - resolve global package manager path for: ${packageManager}`);
    return globalPackageManagerPath.get(packageManager);
}
// The settings have changed. Sent on server activation as well.
connection.onDidChangeConfiguration((params) => {
    globalSettings = params.settings;
    // Clear cache.
    configPathCache = {};
    settingsCache.flush();
    // Revalidate any open text documents.
    validateAllTextDocuments(documents.all());
});
connection.onDidChangeWatchedFiles(() => {
    // Clear cache.
    configPathCache = {};
    validateAllTextDocuments(documents.all());
});
// Listen on the connection.
connection.listen();
//# sourceMappingURL=server.js.map