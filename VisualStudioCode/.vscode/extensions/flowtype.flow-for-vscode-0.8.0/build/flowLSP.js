'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.activate = activate;

var _path = require('path');

var path = _interopRequireWildcard(_path);

var _vscode = require('vscode');

var vscode = _interopRequireWildcard(_vscode);

var _vscodeLanguageclient = require('vscode-languageclient');

var _utils = require('./utils');

var _flowLogging = require('./flowLogging');

var _FlowHelpers = require('./pkg/flow-base/lib/FlowHelpers');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var languages = [{ language: 'javascript', scheme: 'file' }, 'javascriptreact'];

function activate(context) {
  if (!(0, _utils.isFlowEnabled)()) {
    return;
  }
  global.vscode = vscode;

  (0, _flowLogging.setupLogging)();
  (0, _utils.checkNode)();
  (0, _utils.checkFlow)();

  // The server is implemented in node
  var SERVER_HOME = context.asAbsolutePath(path.join('node_modules', 'flow-language-server', 'lib', 'bin', 'cli.js'));

  // If the extension is launched in debug mode then the debug server options are used
  // Otherwise the run options are used
  var serverOptions = {
    run: { module: SERVER_HOME, transport: _vscodeLanguageclient.TransportKind.ipc },
    debug: {
      module: SERVER_HOME,
      transport: _vscodeLanguageclient.TransportKind.ipc,
      options: { execArgv: ['--nolazy', '--debug=6009'] }
    }
  };

  // Options to control the language client
  var clientOptions = {
    documentSelector: languages,
    synchronize: {
      configurationSection: 'flow',
      // Notify the server about file changes to '.clientrc files contain in the workspace
      fileEvents: _vscode.workspace.createFileSystemWatcher('**/*.{js,jsx,mjs,js.flow}')
    }
  };

  var statusBarItem = _vscode.window.createStatusBarItem(_vscode.StatusBarAlignment.Left, 0);
  var serverRunning = false;

  // Create the language client and start the client.
  var client = new _vscodeLanguageclient.LanguageClient('flow', 'Flow', serverOptions, clientOptions);
  var defaultErrorHandler = client.createDefaultErrorHandler();
  var running = 'Flow server is running.';
  var stopped = 'Flow server stopped.';

  client.onDidChangeState(function (event) {
    if (event.newState === _vscodeLanguageclient.State.Running) {
      client.info(running);
      statusBarItem.tooltip = running;
      serverRunning = true;
    } else {
      client.info(stopped);
      statusBarItem.tooltip = stopped;
      serverRunning = false;
    }
    udpateStatusBarVisibility(statusBarItem, serverRunning);
  });

  var disposable = client.start();
  // Push the disposable to the context's subscriptions so that the
  // client can be deactivated on extension deactivation
  context.subscriptions.push(disposable);
}

function udpateStatusBarVisibility(statusBarItem, show) {
  if (show) {
    statusBarItem.show();
    statusBarItem.text = 'Flow';
  } else {
    statusBarItem.hide();
  }
}

_vscode.workspace.onDidChangeConfiguration(function (params) {
  (0, _FlowHelpers.clearWorkspaceCaches)();
});
//# sourceMappingURL=flowLSP.js.map