'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.activate = activate;

var _runtime = require('regenerator-runtime/runtime');

var _ = _interopRequireWildcard(_runtime);

var _vscode = require('vscode');

var _utils = require('./utils');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

// for async/await
function activate(context) {
  if ((0, _utils.useLSP)()) {
    require('./flowLSP').activate(context);
  } else {
    require('./flowMain').activate(context);
  }
}
//# sourceMappingURL=index.js.map