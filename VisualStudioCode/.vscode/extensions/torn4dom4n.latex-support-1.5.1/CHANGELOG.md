# LaTeX language support CHANGELOG

## Version 1.5.0: January 21th, 2018
* Updated `Folding regions`.
* Added grammar for `comment` environment adapt [area/language-latex](https://github.com/area/language-latex) in [#166](https://github.com/area/language-latex/pull/166).

## Version 1.4.0: December 30th, 2017
* Added grammar for `gls` and `glstext` adapt [area/language-latex](https://github.com/area/language-latex) in [#160](https://github.com/area/language-latex/pull/160).
* Fixed `match` is incorrect.

## Version 1.3.0: November 11th, 2017
* Supported [Folding regions](https://code.visualstudio.com/updates/v1_17#_folding-regions). The LaTeX language currently have markers defined: `%Region` and `%Endregion`.
* Added `Expl3` syntax highlight convert from [area/language-latex](https://github.com/area/language-latex).
* Fixes some issues.

## Version 1.2.0: October 29th, 2017
* Snippets are defined in a JSON format.
* Updated `LaTeX`'s extensions.

## Version 1.1.0: October 19th, 2017
* Removed `LaTeX Beamer`, `LaTeX Log`, `LaTeX Memoir` grammars.

## Version 1.0.1: September 27th, 2017
* Updated desciptions and screenshot.

## Version 1.0.0: September 24th, 2017
* Initial release.
